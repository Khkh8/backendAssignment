module.exports = function api(options) {
    let valid_ops = { sum: 'sum', product: 'product' }

    this.add('role:api,path:categories', function (msg, respond) {
        this.act('role:categories', {
        }, respond)
    })

    this.add('role:api,path:note', function (msg, respond) {
        let id = msg.args.params.id
        this.act('role:note', {
            id: id,
        }, respond)
    })

    this.add('role:api,path:notes', function (msg, respond) {
        let category = msg.args.params.category
        this.act('role:notes', {
            category: category,
        }, respond)
    })

    this.add('init:api', function (msg, respond) {
        console.log(`init:api msg:${msg}`)

        this.act('role:web', {
            routes: {
                prefix: '/',
                pin: 'role:api,path:*',
                map: {
                    categories: {
                        GET: true
                    },
                    note: {
                        GET: true,
                        suffix: '/:id'
                    },
                    notes: {
                        GET: true,
                        suffix: '/:category'
                    }
                }
            }
        }, respond)
    })
}