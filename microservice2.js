require('dotenv').config()
const port3 = process.env.PORT3

console.log(`Starting microservice using tcp on port ${port3}`)

require('seneca')()
    .quiet()
    .use(require('./microservice2-plugin'))
    .listen({ type: 'tcp', port: port3 })
