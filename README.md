# Seneca backend assignment
## Application for handling notes
##### Henri Vatto
###### Also available at http://gitlab.com/khkh8/backendAssignment
To use the application you first need to install the dependencies by using the command: 
```powershell
npm install
```
To start the pm2 ecosystem you need to run the command:
```powershell
pm2 start .\ecosystem.config.js
```
Or if pm2 is not installed globally run the command:
```powershell
npx pm2 start .\ecosystem.config.js
```
The paths are as follows
- http://localhost:3000/categories
- http://localhost:3000/notes/:category
- http://localhost:3000/note/:id
- http://localhost:3000/about

Note that the categories are case sensitive!

The Postman collection is included.