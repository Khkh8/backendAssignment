module.exports = function api(options) {
    this.add('role:api,path:about', function (msg, respond) {
        this.act('role:about', {
        }, respond)
    })

    this.add('init:api', function (msg, respond) {

        this.act('role:web', {
            routes: {
                prefix: '/',
                pin: 'role:api,path:*',
                map: {
                    about: {
                        GET: true
                    },
                }
            }
        }, respond)
    })
}