require('dotenv').config()
const port2 = process.env.PORT2

console.log(`Starting microservice using tcp on port ${port2}`)

require('seneca')()
    .quiet()
    .use(require('./microservice-plugin'))
    .listen({ type: 'tcp', port: port2 })
