module.exports = function math(options) {
    const data = require("./data.json")

    this.add('role:categories', (msg, reply) => {
        let response = ""
        for (let i = 0; i < data.Categories.length; i++) {
            response += data.Categories[i].Name + ", "
        }

        reply(null, { "Categories": response });
    });

    // for getting single note with id(1-9)
    this.add('role:note', (msg, reply) => {
        let response
        let id = msg.id
        for (let i = 0; i < data.Categories.length; i++) {
            for (let j = 0; j < data.Categories[i].Items.length; j++) {
                if (data.Categories[i].Items[j].id == id)
                    response = data.Categories[i].Items[j]
            }
        }
        reply(null, { "Note": response });
    });

    this.add('role:notes', (msg, reply) => {
        let response = {}
        let cntr = 0
        let category = msg.category
        for (let i = 0; i < data.Categories.length; i++) {
            if (data.Categories[i].Name === category) {
                for (let j = 0; j < data.Categories[i].Items.length; j++) {
                    response[cntr] = data.Categories[i].Items[j]
                    cntr++
                }
            }
        }
        reply(null, { "Notes": response });
    });
}