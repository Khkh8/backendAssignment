require('dotenv').config()
const SenecaWeb = require('seneca-web')
const express = require('express')
const router = express.Router
const context = new router()
const app = express()
const port = process.env.PORT1
const port2 = process.env.PORT2
const port3 = process.env.PORT3

let senecaWebConfig = {
    context: context,
    adapter: require('seneca-web-adapter-express'),
    options: {
        parseBody: false
    }
}

app.get("/", (req, res) => {
    res.send("App is running")
})

app.use(require("body-parser").json()).use(context).listen(port, () => {
    console.log(`Server running at port ${port}`);
})

let service1 = require('seneca')()
    .quiet()
    .use(SenecaWeb, senecaWebConfig)
    .use(require('./api'))
    .client({ type: 'tcp', port: port2 })

let service2 = require('seneca')()
    .quiet()
    .use(SenecaWeb, senecaWebConfig)
    .use(require('./api2'))
    .client({ type: 'tcp', port: port3 })